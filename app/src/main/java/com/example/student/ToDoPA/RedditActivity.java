package com.example.student.ToDoPA;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

import com.example.student.performanceassessment.R;


public class RedditActivity extends SingleFragmentActivity implements ActivityCallback{

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected Fragment createFragment() {

        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {

        if (findViewById(R.id.detail_fragment_container) == null) {

            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);
        }
        else {
            Fragment detailFragment = RedditWebFragment.newFragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }

    }
}
